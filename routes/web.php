<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

// data obat
Route::get('/crud', 'CrudController@index')->name('crud');
Route::get('/crud/add', 'CrudController@add')->name('cr.t');
Route::get('crud/edit', 'CrudController@edit')->name('cr.e');
Route::post('crud/save', 'CrudController@save')->name('cr.save');
Route::delete('crud/delete/{id}', 'CrudController@delete')->name('cr.delete');
Route::get('crud/{id}/edit', 'CrudController@edit')->name('cr.edit');
Route::patch('crud/{id}', 'CrudController@update')->name('cr.update');

// bagian betuk obat
Route::get('/Bentuk_obat', 'Bentuk_obatController@index')->name('do');
Route::get('/Bentuk_obat/add', 'Bentuk_obatController@add')->name('do.t');
Route::post('/Bentuk_obat/save', 'Bentuk_obatController@save')->name('do.save');
Route::delete('/Bentuk_obat/delete/{id}', 'Bentuk_obatController@delete')->name('do.delete');
Route::get('/Bentuk_obat/{id}/edit', 'Bentuk_obatController@edit')->name('do.edit');
Route::patch('/Bentuk_obat/{id}', 'Bentuk_obatController@update')->name('do.update');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::namespace('Admin')->prefix('admin')->name('admin.')->group(function () {
    Route::resource('/users', 'UserController', ['except' => ['show', 'store']]);
    Route::get('/users/addUser', 'UserController@add')->name('users.t');
    Route::post('/users/save', 'UserController@save')->name('users.save');
    Route::delete('/users/delete/{id}', 'UserController@delete')->name('users.delete');
    Route::get('users/{id}/edit', 'UserController@edit')->name('users.edit');
    Route::patch('users/{id}', 'UserController@update')->name('users.update');
});
