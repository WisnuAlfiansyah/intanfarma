<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name_user' => ['required', 'string', 'max:255'],
            'email_user' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'pass_user' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    private function _validation(Request $request)
    {

        $validation = $request->validate(
            [
                'name' => 'required|string|max:255',
                'email' => 'required|string|max:100',
                'password' => 'required|string|min:8',
            ],
            [

                'name.required' => 'Harus Diisi!',
                'name.max' => 'Maksimal 255 Karakter!',
                'name.min' => 'Minimal 5 Karakter!',

                'email.required' => 'Harus Diisi!',
                'email.max' => 'Maksimal 255 Karakter!',
                'email.min' => 'Minimal 5 Karakter!',

                'password.required' => 'Harus Diisi!',
                'password.max' => 'Maksimal 255 Kotak!',
                'password.min' => 'Minimal 8 Kotak!',

            ]
        );
    }

    public function index()
    {
        $users = User::all();
        return view('admin.users.index')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function add()
    {
        $roles = Role::all();
        return view('admin.users.addUser',['roles' => $roles]);
    }
    public function create()
    {
        // return User::create([
        //     'name_user' => $data['name'],
        //     'email_user' => $data['email'],
        //     'pass_user' => Hash::make($data['password']),
        //     'status' => 1,
        // ]);
        // return redirect()->route('admin.users.index',[
        //     'users' => $users,
        //     'roles' => $roles
        // ])->with('message', 'Data Berhasil Disimpan!');
    }
    public function save(Request $data)
    {
        $user = User::create([
            'name' => $data->nama_user,
            'email' => $data->email_user,
            'status' => 1,
            'password' => Hash::make($data['password_user']),            
        ]);

        $user->assignRole($data->role);

        return redirect()->route('admin.users.index')->with('message', 'Data Berhasil Disimpan!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::all();
        $users = DB::table('users')->where('id', $id)->first();
        return view('admin.users.editUser', [
            'users' => $users,
            'roles' => $roles
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user->roles()->sync($request->role);
        DB::table('users')->where('id', $user->id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request['password']),
        ]);
        return redirect()->route('admin.users.index')->with('message', 'Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        // DB::table('users')->where('id', $user)->delete();
        // return redirect()->back()->with('message', 'Data Berhasil Dihapus!');
    }

    //method hapus data
    public function delete($id)
    {
        DB::table('users')->where('id', $id)->delete();
        return redirect()->back()->with('message', 'Data Berhasil Dihapus!');
    }
}
