<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CrudController extends Controller
{
    //tampilkan data
    public function index()
    {
        $data_obat = DB::table('data_obat')->paginate(10);
        return view('crud', ['data_obat' => $data_obat]);
    }
    public function add()
    {
        $bentuk_obat = DB::table('bentuk_obat')->get();
        return view('crud-tambah-data', ['bentuk_obat' => $bentuk_obat]);
    }

    public function edit($id)
    {
        $data_obat = DB::table('data_obat')
            ->leftjoin('bentuk_obat', 'data_obat.bentuk_obat', '=', 'bentuk_obat.id')
            ->where('data_obat.id', $id)->first();
        $bentuk_obat = DB::table('bentuk_obat')->get();
        return view('crud-edit-data', ['data_obat' => $data_obat, 'bentuk_obat' => $bentuk_obat]);
    }

    private function _validation(Request $request)
    {

        $validation = $request->validate(
            [
                'kode_obat' => 'required|max:10|min:3',
                'nama_obat' => 'required|max:100|min:3',
                'stok_obat' => 'required|integer|between:1,100',
                'bentuk_obat' => 'required',
                'konsumen_obat' => 'required|max:100|min:3',
                'manfaat_obat' => 'required|max:100|min:3',
                'harga_obat' => 'required',
            ],
            [

                'kode_obat.required' => 'Harus Diisi!',
                'kode_obat.max' => 'Maksimal 10 Karakter!',
                'kode_obat.min' => 'Minimal 3 Karakter!',

                'nama_obat.required' => 'Harus Diisi!',
                'nama_obat.max' => 'Maksimal 100 Karakter!',
                'nama_obat.min' => 'Minimal 3 Karakter!',

                'stok_obat.required' => 'Harus Diisi!',

                'bentuk_obat.required' => 'Harus Diisi!',

                'konsumen_obat.required' => 'Harus Diisi!',
                'konsumen_obat.max' => 'Maksimal 200 Kotak!',
                'konsumen_obat.min' => 'Minimal 1 Kotak!',

                'manfaat_obat.required' => 'Harus Diisi!',
                'manfaat_obat.max' => 'Maksimal 200 Kotak!',
                'manfaat_obat.min' => 'Minimal 1 Kotak!',

                'harga_obat.required' => 'Harus Diisi!',
            ]
        );
    }

    //method simpan
    public function save(Request $request)
    {

        //   dd($request->all());
        $this->_validation($request);
        DB::table('data_obat')->insert([
            'kode_obat' => $request->kode_obat,
            'nama_obat' => $request->nama_obat,
            'stok_obat' => $request->stok_obat,
            'bentuk_obat' => $request->bentuk_obat,
            'konsumen_obat' => $request->konsumen_obat,
            'manfaat_obat' => $request->manfaat_obat,
            'harga_obat' => $request->harga_obat
        ]);

        return redirect()->route('crud')->with('message', 'Data Berhasil Disimpan!');
    }

    //method hapus data
    public function delete($id)
    {
        DB::table('data_obat')->where('id', $id)->delete();
        return redirect()->back()->with('message', 'Data Berhasil Dihapus!');
    }


    public function update(Request $request, $id)
    {
        //  dd($request->all());
        $this->_validation($request);
        DB::table('data_obat')->where('id', $id)->update([
            'kode_obat' => $request->kode_obat,
            'nama_obat' => $request->nama_obat,
            'stok_obat' => $request->stok_obat,
            'bentuk_obat' => $request->bentuk_obat,
            'konsumen_obat' => $request->konsumen_obat,
            'manfaat_obat' => $request->manfaat_obat,
            'harga_obat' => $request->harga_obat,
        ]);
        return redirect()->route('crud')->with('message', 'Data Berhasil Diubah');
    }
}
