<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Bentuk_obatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bentuk_obat = DB::table('bentuk_obat')->paginate(10);
        // dd($bentuk_obat);
        return view('bentuk_obat.index', ['bentuk_obat' => $bentuk_obat]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('bentuk_obat.tambah');
    }

    private function _validation(Request $request)
    {

        $validation = $request->validate(
            [
                'bentuk' => 'required|max:100|min:3',
            ],
            [

                'bentuk.required' => 'Harus Diisi!',
            ]
        );
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->_validation($request);
        DB::table('bentuk_obat')->insert([
            'bentuk' => $request->bentuk,
        ]);
        return redirect()->route('do')->with('message', 'Data Berhasil Disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bentuk_obat = DB::table('bentuk_obat')->where('id', $id)->first();
        return view('bentuk_obat.edit', ['bentuk_obat' => $bentuk_obat]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->_validation($request);
        DB::table('bentuk_obat')->where('id', $id)->update([
            'bentuk' => $request->bentuk,
        ]);
        return redirect()->route('do')->with('message', 'Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        DB::table('bentuk_obat')->where('id', $id)->delete();
        return redirect()->back()->with('message', 'Data Berhasil Dihapus!');
    }
}
