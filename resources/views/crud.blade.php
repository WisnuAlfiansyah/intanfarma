@extends('layouts.master')
@section('title','')


@section('content')
<div class="section-body">
  <div class="row" >
    <div class="col-12 col-md-12 col-lg-12">
      <div class="buttons">
        <a href="{{route('cr.t')}}" class="btn btn-outline-primary">Tambah Data+</a>
        <a href="{{route('do')}}" class="btn btn-outline-primary">Data Bentuk Obat</a>
        <hr>
        @if (session('message'))
        <div class="col-12 col-md-12 col-lg-12 alert alert-success alert-has-icon">
          <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
          <div class="alert-body">
            <div class="alert-title">Success</div>
            {{session('message')}}
          </div>
        </div>

        @endif
        <table class="table table-striped table-bordered">
          <tr>
            <th>No.</th>
            <th scope="col">Kode Obat</th>
            <th scope="col">Nama Obat</th>
            <th scope="col">Stok</th>
            <th scope="col">Bentuk</th>
            <th scope="col">Konsumen</th>
            <th scope="col">Manfaat</th>
            <th scope="col">Harga</th>
            <th scope="col">Aksi</th>
          </tr>
          @foreach ($data_obat as $no => $data)
          <tr>
            <td>{{$data_obat->firstItem()+$no}}</td>
            <td>{{$data->kode_obat}}</td>
            <td>{{$data->nama_obat}}</td>
            <td>{{$data->stok_obat}} Kotak</td>
            <td>{{$data->bentuk_obat}}</td>
            <td>{{$data->konsumen_obat}}</td>
            <td>{{$data->manfaat_obat}}</td>
            <td>Rp.{{$data->harga_obat}}</td>


            <td>
              <a type="button" href="{{route('cr.edit',$data->id)}}" class="badge badge-warning">Ubah</a>
              {{-- <a href="#"  data-id="{{$data->id}}" class="badge badge-danger swal-1"> --}}
                <form  id="delete{{$data->id}}" action="{{route('cr.delete',$data->id)}}" method="POST">
                @csrf
                @method('delete')
                {{-- <input type="button" value="Delete" class="badge badge-danger swal-1"/> --}}
                <button class="badge badge-danger swal-1" style="border: none"> delete </button>
                </form>
            </td>
          </tr>
          @endforeach



        </table>
        {{$data_obat->links()}}
      </div>

    </div>


  </div>
</div>



@endsection
@push('page-scripts')
       <script src="{{asset('assets/js/page/modules-sweetalert.js')}}"> </script>
       <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endpush

@push('after-scripts')
<script>
$(".swal-1").click(function(e) {
    id = e.target.dataset.id;
    swal({
        title: 'Yakin Hapus Data?',
        text: 'Data yang dihapus tidak bisa dikembalikan!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
       swal('Poof! Hapus Data!', {
         icon: 'success',
        });
        $(`#delete${id}`).submit();
        } else {
          swal('Batal Hapus Data!');
        }
      });
  });
</script>

@endpush


