<div class="form-group">
    <label>{{$label ?? ''}}</label>
    <input label = "{{$label}}"  for="{{$field}}" value="{{old($field)}}"  name="{{$field}}" type="text" class="form-control" placeholder="{{$placeholder}}"
    @isset($prevvalue) value="{{old($field) ? old($field): $prevvalue }}"
    @else value="{{old($field)}}"
    @endisset
    
    
    >
  </div>
