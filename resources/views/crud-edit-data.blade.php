@extends('layouts.master')
@section('title','')


@section('content')
<div class="section-body">
</div>

<div class="buttons">
    <div class="card">
        <div class="card-header">
          <h4>Data Barang</h4>
        </div>
        <div class="card-body">
          <div class="alert alert-info">
            <b>Note!</b> Pastikan Isi data dengan Baik dan Benar!
          </div>
         <form action="{{route('cr.update',$data_obat->id)}}"  method="POST" enctype="multipart/form-data">
          @csrf
          @method ('patch')
          <div class="form-group">
            <label for="kode_obat" @error('kode_obat')
                class="text-danger"
            @enderror> Kode Obat @error('kode_obat')
               {{$message}}
              @enderror</label>
            <input name="kode_obat"  type="text"
            @if (old('kode_obat'))
              value="{{old('kode_obat')}}"
            @else
              value="{{$data_obat->kode_obat}}"
            @endif
            class="form-control" ></input>
          </div>

          <div class="form-group">
            <label for="nama_obat" @error('nama_obat')
                class="text-danger"
            @enderror> Nama Obat @error('nama_obat')
               {{$message}}
              @enderror</label>
            <input name="nama_obat"  type="text"
            @if (old('nama_obat'))
              value="{{old('nama_obat')}}"
            @else
              value="{{$data_obat->nama_obat}}"
            @endif
            class="form-control" ></input>
          </div>

          <div class="form-group">
            <label for="stok_obat" @error('stok_obat')
                class="text-danger"
            @enderror> Stok Obat @error('stok_obat')
               {{$message}}
              @enderror</label>
            <input name="stok_obat"  type="text"
            @if (old('stok_obat'))
              value="{{old('stok_obat')}}"
            @else
              value="{{$data_obat->stok_obat}}"
            @endif
            class="form-control" ></input>
          </div>

          <div class="form-group">
            <label for="bentuk_obat" @error('bentuk_obat')
            class="text-danger"
            @enderror> Bentuk Obat @error('bentuk_obat')
            {{$message}}
            @enderror</label>
                <select class="form-control" name="bentuk_obat" value="bentuk_obat">
                    <option value="{{$data_obat->bentuk_obat}}">{{$data_obat->bentuk}}</option>
                    @foreach ($bentuk_obat as $item)
                        @if ( $item->id != $data_obat->bentuk_obat)
                            <option value="{{$item->id}}">{{$item->bentuk}}</option>
                        @endif
                    @endforeach
                </select>
        </div>

          <div class="form-group">
            <label for="konsumen_obat" @error('konsumen_obat')
                class="text-danger"
            @enderror> Konsumen Obat @error('konsumen_obat')
               {{$message}}
              @enderror</label>
            <input name="konsumen_obat"  type="text"
            @if (old('konsumen_obat'))
              value="{{old('konsumen_obat')}}"
            @else
              value="{{$data_obat->konsumen_obat}}"
            @endif
            class="form-control" ></input>
          </div>

          <div class="form-group">
            <label for="manfaat_obat" @error('manfaat_obat')
                class="text-danger"
            @enderror> Manfaat Obat @error('manfaat_obat')
               {{$message}}
              @enderror</label>
            <input name="manfaat_obat"  type="text"
            @if (old('manfaat_obat'))
              value="{{old('manfaat_obat')}}"
            @else
              value="{{$data_obat->manfaat_obat}}"
            @endif
            class="form-control" ></input>
          </div>

          <div class="form-group">
            <label for="harga_obat" @error('harga_obat')
                class="text-danger"
            @enderror> Harga Obat @error('harga_obat')
               {{$message}}
              @enderror</label>
            <input name="harga_obat"  type="text"
            @if (old('harga_obat'))
              value="{{old('harga_obat')}}"
            @else
              value="{{$data_obat->harga_obat}}"
            @endif
            class="form-control" ></input>
          </div>
        </div>
        <div class="card-footer text-right">
          <button class="btn btn-primary mr-1" type="submit">Submit</button>
          <button class="btn btn-primary mr-1" type="submit">Submit</button>
          <button class="btn btn-secondary" type="reset">Reset</button>
        </div>
      </form>
      </div>
    </div>


  </div>
@endsection
@push('page-scripts')

@endpush
