@extends('layouts.master')
@section('title','')


@section('content')
<div class="section-body">
</div>

<div class="buttons">
    <div class="card">
        <div class="card-header">
          <h4>Data Barang</h4>
        </div>
        <div class="card-body">
          <div class="alert alert-info">
            <b>Note!</b> Pastikan Isi data dengan Baik dan Benar!
          </div>
         <form action="{{route('cr.save')}}" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="form-group">
            <label for="kode_obat" @error('kode_obat')
                class="text-danger"
            @enderror> Kode Obat @error('kode_obat')
               {{$message}}
              @enderror</label>
            <x-input field="kode_obat" label="" type="text" class="form-control" value="kode_obat" placeholder="Kode Obat.."></x-input>
          </div>

          <div class="form-group">
            <label for="nama_obat" @error('nama_obat')
                class="text-danger"
            @enderror> Nama Obat @error('nama_obat')
               {{$message}}
              @enderror</label>
            <x-input field="nama_obat" label="" type="text" value="nama_obat" class="form-control" placeholder="Nama Obat.."></x-input>
          </div>

          <div class="form-group">
            <label for="stok_obat" @error('stok_obat')
                class="text-danger"
            @enderror> Stok Obat @error('stok_obat')
               {{$message}}
              @enderror</label>
            <x-input field="stok_obat" label="" type="text" class="form-control" placeholder="Stok Obat.."></x-input>
          </div>

         <div class="form-group">
                <label for="bentuk_obat" @error('bentuk_obat')
                class="text-danger"
                @enderror> Bentuk Obat @error('bentuk_obat')
            {{$message}}
            @enderror</label>
                <select class="form-control" name="bentuk_obat" value="bentuk_obat">
                    <option value="">-- select one --</option>
                    @foreach ($bentuk_obat as $item)
                        <option value="{{$item->id}}">{{$item->bentuk}}</option>
                    @endforeach
                </select>
          </div>

          <div class="form-group">
            <label for="konsumen_obat" @error('konsumen_obat')
                class="text-danger"
            @enderror> Konsumen Obat @error('konsumen_obat')
               {{$message}}
              @enderror</label>
            <x-input field="konsumen_obat" label="" type="text" value="konsumen_obat" class="form-control" placeholder="Konsumen Obat.."></x-input>
          </div>

          <div class="form-group">
            <label for="manfaat_obat" @error('manfaat_obat')
                class="text-danger"
            @enderror> Manfaat Obat @error('manfaat_obat')
               {{$message}}
              @enderror</label>
            <x-input field="manfaat_obat" label="" type="text" value="manfaat_obat" class="form-control" placeholder="Manfaat Obat.."></x-input>
          </div>

          <div class="form-group">
            <label for="harga_obat" @error('harga_obat')
                class="text-danger"
            @enderror> Harga Obat @error('harga_obat')
               {{$message}}
              @enderror</label>
            <x-input field="harga_obat" label="" type="text" value="harga_obat" class="form-control" placeholder="Harga Obat.."></x-input>
          </div>
        </div>
        <div class="card-footer text-right">
          <button class="btn btn-primary mr-1" type="submit">Submit</button>
          <button class="btn btn-secondary" type="reset">Reset</button>
        </div>
      </form>
      </div>
    </div>
  </div>
@endsection
@push('page-scripts')
@endpush
