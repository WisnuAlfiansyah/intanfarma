@extends('layouts.master')
@section('title','')


@section('content')
<div class="section-body">
</div>

<div class="buttons">
    <div class="card">
        <div class="card-header">
          <h4>Tambah User</h4>
        </div>
        <div class="card-body">
          <div class="alert alert-info">
            <b>Note!</b> Pastikan Isi data dengan Baik dan Benar!
          </div>
         <form action="{{route('admin.users.save')}}" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="form-group">
            <label for="nama_user" @error('nama_user')
                class="text-danger"
            @enderror> Nama User @error('nama_user')
               {{$message}}
              @enderror</label>
            <x-input field="nama_user" label="" type="text" class="form-control" value="nama_user" placeholder="Nama User.."></x-input>
          </div>

          <div class="form-group">
            <label for="email_user" @error('email_user')
                class="text-danger"
            @enderror> Email User @error('email_user')
               {{$message}}
              @enderror</label>
            <x-input field="email_user" label="" type="text" value="email_user" class="form-control" placeholder="Email User.."></x-input>
          </div>

          <div class="form-group">
            <label for="pass_user" @error('pass_user')
                class="text-danger"
            @enderror> Password User @error('pass_user')
               {{$message}}
              @enderror</label>
            <x-input field="pass_user" label="" type="text" class="form-control" placeholder="Password User.."></x-input>
          </div>
        </div>
        <div class="form-group">
            <label for="password" @error('password') class="text-danger" @enderror> Role @error('password') {{$message}} @enderror</label>
            <div class="form-check">
              
              @foreach ($roles as $role)
              <input type="checkbox" value="{{$role->id}}" name="role[]">
              <label>{{$role->name}}</label>
              @endforeach
            </div>
          </div>
        <div class="card-footer text-right">
          <button class="btn btn-primary mr-1" type="submit">Submit</button>
          <button class="btn btn-secondary" type="reset">Reset</button>
        </div>
      </form>
      </div>
    </div>
  </div>
@endsection
@push('page-scripts')
@endpush
