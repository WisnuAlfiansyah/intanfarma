@extends('layouts.master')
@section('title','')


@section('content')
<div class="section-body">
</div>

<div class="buttons">
    <div class="card">
        <div class="card-header">
          <h4>Data User</h4>
        </div>
        <div class="card-body">
          <div class="alert alert-info">
            <b>Note!</b> Pastikan Isi data dengan Baik dan Benar!
          </div>
         <form action="{{route('admin.users.update',$users->id)}}"  method="POST" enctype="multipart/form-data">
          @csrf
          @method ('patch')
          <div class="form-group">
            <label for="name" @error('nama_user')
                class="text-danger"
            @enderror> Nama User @error('name')
               {{$message}}
              @enderror</label>
            <input name="name"  type="text"
            @if (old('name'))
              value="{{old('name')}}"
            @else
              value="{{$users->name}}"
            @endif
            class="form-control" ></input>
          </div>

          <div class="form-group">
            <label for="email" @error('email')
                class="text-danger"
            @enderror> Email User @error('email')
               {{$message}}
              @enderror</label>
            <input name="email"  type="text"
            @if (old('email'))
              value="{{old('email')}}"
            @else
              value="{{$users->email}}"
            @endif
            class="form-control" ></input>
          </div>

          <div class="form-group">
            <label for="password" @error('password')
                class="text-danger"
            @enderror> Password User @error('password')
               {{$message}}
              @enderror</label>
            <input name="password"  type="text"
            @if (old('password'))
              value="{{old('password')}}"
            @else
              value="{{$users->password}}"
            @endif
            class="form-control" ></input>
          </div>

          <div class="form-group">
            <label for="password" @error('password') class="text-danger" @enderror> Role @error('password') {{$message}} @enderror</label>
            <div class="form-check">
              
              @foreach ($roles as $role)
              <input type="checkbox" value="{{$role->id}}" name="role[]">
              <label>{{$role->name}}</label>
              @endforeach
            </div>
          </div>

        <div class="card-footer text-right">
          <button class="btn btn-primary mr-1" type="submit">Submit</button>
          <button class="btn btn-secondary" type="reset">Reset</button>
        </div>
      </form>
      </div>
    </div>


  </div>
@endsection
@push('page-scripts')

@endpush
