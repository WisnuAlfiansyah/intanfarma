@extends('layouts.master')
@section('title','')


@section('content')
<div class="section-body">
  <div class="row" >
    <div class="col-12 col-md-12 col-lg-12">
      <div class="buttons">
        <a href="{{route('admin.users.t')}}" class="btn btn-outline-primary">Tambah Data+</a>
        <table class="table table-striped table-bordered">
          <tr>
            <th>No.</th>
            <th scope="col">Nama </th>
            <th scope="col">Email</th>
            <!-- <th scope="col">Roles</th> -->
            <th scope="col">Aksi</th>
          </tr>
          @php $no = 1; @endphp
          @foreach ($users as $data)
          <tr>
            <td>{{$no++}}</td>
            <td>{{$data->name}}</td>
            <td>{{$data->email}}</td>
            <td>{{ implode(',', $data->roles()->get()->pluck('name')->toArray()) }}</td>
            <td>
              <a type="button" href="{{route('admin.users.edit',$data->id)}}" class="badge badge-warning">Ubah</a>
              {{-- <a href="#"  data-id="{{$data->id}}" class="badge badge-danger swal-1"> --}}
                <form  id="delete{{$data->id}}" action="{{route('admin.users.delete',$data->id)}}" method="POST">
                @csrf
                @method('delete')
                {{-- <input type="button" value="Delete" class="badge badge-danger swal-1"/> --}}
                <button class="badge badge-danger swal-1" style="border: none"> delete </button>
                </form>
            </td>
          </tr>
          @endforeach
        </table>
      </div>

    </div>


  </div>
</div>



@endsection
@push('page-scripts')
       <script src="{{asset('assets/js/page/modules-sweetalert.js')}}"> </script>
       <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endpush

@push('after-scripts')
<script>
$(".swal-1").click(function(e) {
    id = e.target.dataset.id;
    swal({
        title: 'Yakin Hapus Data?',
        text: 'Data yang dihapus tidak bisa dikembalikan!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
       swal('Poof! Hapus Data!', {
         icon: 'success',
        });
        $(`#delete${id}`).submit();
        } else {
          swal('Batal Hapus Data!');
        }
      });
  });
</script>

@endpush
