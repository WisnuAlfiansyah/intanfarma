@extends('layouts.master')
@section('title','')


@section('content')
<div class="section-body">
  <div class="row" >
    <div class="col-12 col-md-12 col-lg-12">
      <div class="buttons">
        <a href="{{route('do.t')}}" class="btn btn-outline-primary">Tambah Data + </a>
        <hr>
        @if (session('message'))
        <div class="col-12 col-md-12 col-lg-12 alert alert-success alert-has-icon">
          <div class="alert-icon"><i class="far fa-lightbulb"></i></div>
          <div class="alert-body">
            <div class="alert-title">Success</div>
            {{session('message')}}
          </div>
        </div>

        @endif
        <table class="table table-striped table-bordered">
          <tr>
            <th width='10px'>No.</th>
            <th scope="col">Bentuk Obat</th>
            <th scope="col">Aksi</th>
          </tr>
          @foreach ($bentuk_obat as $no => $data)
          <tr>
            <td width='10px'>{{$bentuk_obat->firstItem()+$no}}</td>
            <td>{{$data->bentuk}}</td>
            <td>
              <a type="button" href="{{route('do.edit',$data->id)}}" class="badge badge-warning">Ubah</a>
                <form  id="delete{{$data->id}}" action="{{route('do.delete',$data->id)}}" method="POST">
                @csrf
                @method('delete')
                <button class="badge badge-danger swal-1" style="border: none"> delete </button>
                </form>
            </td>
          </tr>
          @endforeach



        </table>
        {{$bentuk_obat->links()}}
      </div>

    </div>


  </div>
</div>



@endsection
@push('page-scripts')
       <script src="{{asset('assets/js/page/modules-sweetalert.js')}}"> </script>
       <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endpush

@push('after-scripts')
<script>
$(".swal-1").click(function(e) {
    id = e.target.dataset.id;
    swal({
        title: 'Yakin Hapus Data?',
        text: 'Data yang dihapus tidak bisa dikembalikan!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
       swal('Poof! Hapus Data!', {
         icon: 'success',
        });
        $(`#delete${id}`).submit();
        } else {
          swal('Batal Hapus Data!');
        }
      });
  });
</script>

@endpush


