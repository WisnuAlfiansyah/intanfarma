@extends('layouts.master')
@section('title','')


@section('content')
<div class="section-body">
</div>

<div class="buttons">
    <div class="card">
        <div class="card-header">
          <h4>Data Bentuk Obat</h4>
        </div>
        <div class="card-body">
          <div class="alert alert-info">
            <b>Note!</b> Pastikan Isi data dengan Baik dan Benar!
          </div>
         <form action="{{route('do.save')}}" method="POST" enctype="multipart/form-data">
          @csrf

          <div class="form-group">
            <label for="kode_obat" @error('bentuk')
                class="text-danger"
            @enderror> Bentuk Obat @error('bentuk')
               {{$message}}
              @enderror</label>
            <x-input field="bentuk" label="" type="text" class="form-control" value="bentuk" placeholder="Bentuk Obat.."></x-input>
          </div>
        </div>
        <div class="card-footer text-right">
          <button class="btn btn-primary mr-1" type="submit">Submit</button>
          <a href="{{route('do')}}"><button class="btn btn-danger mr-1" type="button">Cencel</button></a>
        </div>
      </form>
      </div>
    </div>
  </div>
@endsection
@push('page-scripts')
@endpush
