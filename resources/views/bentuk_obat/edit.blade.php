@extends('layouts.master')
@section('title','')


@section('content')
<div class="section-body">
</div>

<div class="buttons">
    <div class="card">
        <div class="card-header">
          <h4>Edit Bentuk Obat</h4>
        </div>
        <div class="card-body">
          <div class="alert alert-info">
            <b>Note!</b> Pastikan Isi data dengan Baik dan Benar!
          </div>
         <form action="{{route('do.update',$bentuk_obat->id)}}" method="POST" enctype="multipart/form-data">
          @csrf
          @method ('patch')
          <div class="form-group">
              <div class="form-group">
                <label for="kode_obat" @error('bentuk')
                    class="text-danger"
                @enderror> Kode Obat @error('bentuk')
                   {{$message}}
                  @enderror</label>
                <input name="bentuk"  type="text"
                @if (old('bentuk'))
                  value="{{old('bentuk')}}"
                @else
                  value="{{$bentuk_obat->bentuk}}"
                @endif
                class="form-control" />
              </div>
          </div>
        </div>
        <div class="card-footer text-right">
          <button class="btn btn-primary mr-1" type="submit">Submit</button>
          <a href="{{route('do')}}"><button class="btn btn-danger mr-1" type="button">Cencel</button></a>
        </div>
      </form>
      </div>
    </div>
  </div>
@endsection
@push('page-scripts')
@endpush
